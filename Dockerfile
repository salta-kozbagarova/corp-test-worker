FROM node:8.11

ENV \
    QUIZ_DB_PROTOCOL='postgres://' \
    QUIZ_DB_HOST='assets-db' \
    QUIZ_DB_USER='assets' \
    QUIZ_DB_PASSWORD='assets' \
    QUIZ_DB_PORT='5432' \
    QUIZ_DB_DBNAME='assets'

WORKDIR /app

# libs
COPY package.json /app
COPY package-lock.json /app
RUN npm install

# main files
COPY *.js /app/
COPY lib /app/lib

CMD node index.js
