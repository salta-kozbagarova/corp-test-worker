#/bin/bash

RUN_ONCE='true' \
QUIZ_DB_PROTOCOL='postgres://' \
QUIZ_DB_HOST='localhost' \
QUIZ_DB_PORT='35432' \
QUIZ_DB_DBNAME='assets' \
QUIZ_DB_USER='assets' \
QUIZ_DB_PASSWORD='assets' \
node index.js
