#/bin/bash

TYPE=$1
VERSION=$2
if [ -z "$VERSION" ]; then
	VERSION="1.0.0"
fi
if [ "$TYPE" = "latest" ]; then
    BASEDIR=`pwd`
    cd "$BASEDIR"
    echo "Building $VERSION-$TYPE image"
    docker build -t registry.gitlab.com/salta-kozbagarova/corp-test-worker:$VERSION-$TYPE .
    echo "Pushing $VERSION-$TYPE image to repository"
    docker push registry.gitlab.com/salta-kozbagarova/corp-test-worker:$VERSION-$TYPE
else
    echo "TYPE argument is missing: latest. Ex: ./docker-build.sh latest 1.0.0"
fi
