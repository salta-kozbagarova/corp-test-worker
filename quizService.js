const dbConnect = require('./lib/dbConnectPG');
const db = dbConnect.db;


function shuffle(a) {
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

async function getQuestions(filters) {
    try{
        let sqlQuery = 'SELECT * FROM question WHERE is_active = $1';
        let params = [true];
        if(filters['offset'] >= 0 && filters['limit'] >= 0){
            sqlQuery += ' offset $2 limit $3';
            params.push(filters['offset']);
            params.push(filters['limit']);
        }
        let res = null;
        await db.any(sqlQuery, params)
            .then(function(data) {
                res = data;
            })
            .catch(function(error) {
                console.log('error');
                console.log(error);
            });
        return res;
    } catch (e) {
        return [];
    }
}

async function createNewTest(){
  try {
    let res = null;
    let seqQuery = 'select nextval($1)';
    let offset = null;
    await db.one(seqQuery, ['question_seq'])
        .then(function(data) {
          offset = parseInt(data['nextval']);
        })
        .catch(function(error) {
          offset = 0;
          console.log('error');
          console.log(error);
        });
    let rowNum = 0;
    await db.one('select count(id) as row_num from corp_test')
        .then(function(data) {
          rowNum = parseInt(data['row_num']);
        })
        .catch(function(error) {
          rowNum = 0;
          console.log('error');
          console.log(error);
        });
    rowNum += 1;
    const quests = await getQuestions({offset: offset, limit: 10});
    let qs = [];
    for(let i in quests){
      quests[i]['config']['choices'] = shuffle(quests[i]['config']['choices']);
      qs.push(quests[i]['config']);
    }
    await db.one('INSERT INTO corp_test(name, config) VALUES($1, $2::json) RETURNING *', ['Тест №' + rowNum.toString(), JSON.stringify(qs)])
        .then(data => {
          res = data;
        })
        .catch(error => {
          res = {};
          console.log('ERROR:', error); // print error;
        });
    return res;
  } catch (e) {
    return 1;
  }
}

module.exports = {
  createNewTest
};
