const cron = require("node-cron");
const _lg = console.log

const quizService = require("./quizService");


async function createNewTestWrapper() {
    try {
        _lg('Running quizService.createNewTest()')
        await quizService.createNewTest();
        _lg('Done with quizService.createNewTest()')

    } catch (error) {
        _lg('failed to create new test')
        _lg(error)
    }
}


if (process.env.RUN_ONCE === 'true') {
    createNewTestWrapper()

} else {
    _lg('Creating cron task')
    var createNewTestTask = cron.schedule(
        "0 0 10 * * 6",
        // "0 * * * * *",
        createNewTestWrapper, {
            scheduled: true,
            timezone: 'Asia/Almaty'
        }
    );

    //start Cron Job
    _lg('Scheduling cron task')
    createNewTestTask.start();
}
