const initOptions = {
    schema: 'quiz' /* can also be an array of strings or a callback */
};
var pgp = require("pg-promise")(initOptions);
const connectionStr = `${process.env.QUIZ_DB_PROTOCOL}${process.env.QUIZ_DB_USER}:${process.env.QUIZ_DB_PASSWORD}@${process.env.QUIZ_DB_HOST}:${process.env.QUIZ_DB_PORT}/${process.env.QUIZ_DB_DBNAME}`;
var db = pgp(connectionStr);

module.exports = {
    db
};
